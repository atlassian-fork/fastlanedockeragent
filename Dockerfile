FROM docker.atl-paas.net/buildeng/agent-baseagent


RUN apt-get update \
	&& apt-get upgrade -y \
	&& apt-get install -y ruby ruby-dev \
	&& gem install fastlane  

COPY setup.sh /buildeng-custom/setup.sh
